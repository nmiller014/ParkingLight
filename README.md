# Parking Light

This project is a simple go/stop light for proper parking distance in the garage.
It uses a neopixel strip as the red/yellow/green light, and an HC-SR04
distance sensor to determine distance. It measures the distance of your car from
the wall and shows blue while you're not in the garage, green while you're far
from the wall, yellow as you get closer, and red when you should stop.

Wiring:

HC-SR04 VCC to 5V  
HC-SR04 GND to ground  
HC-SR04 TRIG to pin 7 on the arduino  
HC-SR04 ECHO to pin 5 on the arduino  
  
Neopixel DIN to pin 10 on the arduino  
Neopixel VCC to 5V  
Neopixel GND to ground  
  
Arduino VCC to 5V  
Arduino GND to ground  
  
5V and ground should go to some 5-volt power source. I used a micro usb breakout
from sparkfun.

Some pictures of my prototype setup:  
[Front](http://imgur.com/xf3VTy2)  
[Back](http://imgur.com/HuX4bdx)  