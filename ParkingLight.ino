#include <Adafruit_NeoPixel.h>

#define TRIGGER_PIN (7)
#define ECHO_PIN (5)
#define MAX_DISTANCE_PULSE_TIME_MS (23200)
#define CENTIMETER_DIVISOR (58)

#define NEOPIXEL_PIN (10)
#define PIXEL_COUNT (8)

#define STOP_DISTANCE_CM (76)
#define WARN_DISTANCE_CM (183)

Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, NEOPIXEL_PIN, (NEO_GRB + NEO_KHZ800));

static void setAllPixels(uint8_t r, uint8_t g, uint8_t b)
{
  for (uint8_t i = 0; i < PIXEL_COUNT; i++)
  {
     strip.setPixelColor(i, r, g, b);
  }
  strip.show();
}

void setup()
{
  pinMode(TRIGGER_PIN, OUTPUT);
  digitalWrite(TRIGGER_PIN, LOW);

  Serial.begin(9600);

  strip.begin();
  setAllPixels(0, 0, 0);
  
  Serial.println("Ready");
}

void loop()
{
  uint32_t t1;
  uint32_t t2;
  uint32_t pulse_width;
  uint32_t cm;

  // Hold the trigger pin high for at least 10 us
  digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER_PIN, LOW);

  // Wait for pulse on echo pin
  while (digitalRead(ECHO_PIN) == LOW);

  // Measure how long the echo pin was held high (pulse width)
  // Note: the micros() counter will overflow after ~70 min
  t1 = micros();
  while (digitalRead(ECHO_PIN) == HIGH);
  t2 = micros();

  pulse_width = (t2 - t1);
  cm = ((pulse_width * 1000) / CENTIMETER_DIVISOR) / 1000;

  // Print out results
  if (pulse_width > MAX_DISTANCE_PULSE_TIME_MS)
  {
    setAllPixels(0, 0, 255);
  }
  else
  {
    if (cm < STOP_DISTANCE_CM)
    {
      setAllPixels(255, 0, 0);
    }
    else if (cm < WARN_DISTANCE_CM)
    {
      setAllPixels(255, 125, 0);
    }
    else
    {
      setAllPixels(0, 255, 0);
    }
  }

  delay(100);
}
